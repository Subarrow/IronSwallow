import pytz
import requests
import shutil
import csv
import os
import json
import logging
from datetime import datetime, timedelta
from zipfile import ZipFile
from collections import OrderedDict

from sqlalchemy import select, update, text
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import Session

from IronSwallowORM import models
from ironswallow.util import config
from ironswallow.util.darwin_util import full_original_wt

log = logging.getLogger("IronSwallow")

# TODO: MOVE TO CONFIG
from ironswallow.util.database import DatabaseConnection

DFT_GTFS_URL = "https://data.bus-data.dft.gov.uk/timetable/download/gtfs-file/london/"


GTFS_CATEGORIES = {
    "100": ("S", "OO"),  # Railway service
    "101": ("S", "XX"),  # HSR
    "102": ("S", "OO"),  # Long-distance
    "103": ("S", "OO"),  # Interregional
    "104": ("S", "XR"),  # Motorail
    "105": ("S", "XZ"),  # Sleeper rail
    "106": ("S", "OO"),  # Regional railways
    "107": ("M", "OO"),  # Tourist railways
    "108": ("M", "OO"),  # Internal rail shuttle
    "109": ("S", "OO"),  # Suburban railway
    "110": ("S", "OO"),  # Replacement rail service
    "111": ("S", "OO"),  # Special rail service
    "112": ("-", "ZZ"),  # Lorry transport rail service
    "113": ("S", "OO"),  # All rail services
    "114": ("S", "OO"),  # Crosscountry
    "115": ("-", "ZZ"),  # Vehicle transport rail
    "116": ("M", "OO"),  # Rack and pinion
    "117": ("S", "OO"),  # Additional rail service

    "400": ("M", "OL"),  # Urban
    "401": ("M", "OL"),  # Metro
    "402": ("M", "OL"),  # Underground rail
    "403": ("M", "OL"),  # Urban
    "404": ("M", "OL"),  # All urban
    "405": ("M", "OL"),  # Monorail

    "1100": ("A", "ZZ"), # Air
}

ISO_WEEKDAYS = [None, "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]


def time_as_secs(time_str):
    time_parts = time_str.split(":")
    return int(time_parts[0])*3600 + int(time_parts[1])*60 + int(time_parts[2])


def dt_as_day_secs(dt: datetime):
    return dt.second + dt.minute*60 + dt.hour * 3600

def day_secs_as_dt(base_dt: datetime, day_secs):
    return datetime(base_dt.year, base_dt.month, base_dt.day) + timedelta(seconds=day_secs)


def category_for(gtfs_row: dict):
    if gtfs_row.get("parent_station") and gtfs_row.get("platform_code"):
        return "P"
    if gtfs_row.get("vehicle_type"):
        return GTFS_CATEGORIES.get(gtfs_row["vehicle_type"], ("-", "ZZ"))[0]

    return "-"


def _unbommed_dictreader(file_handle):
    dict_reader = csv.DictReader(file_handle)
    dict_reader.fieldnames = [fieldname.replace("\uFEFF", "") for fieldname in dict_reader.fieldnames]
    return dict_reader


def store(db_c: DatabaseConnection, file_location, feed_params: dict) -> None:
    utcnow = datetime.utcnow()
    agency_tz = None
    version_ident = feed_params["id"] + ":" + utcnow.strftime("%Y%m%d%H%M%S")

    if feed_params.get("location_from_gtfs", True):
        with open(file_location + "stops.txt") as f:
            for stop in _unbommed_dictreader(f):
                print(stop)
                prefix = feed_params["location_namespace"] + ":"
                stop["stop_id"] = stop["stop_id"].replace(feed_params.get("location_left_strip", ""), "", 1)
                if "parent_station" in stop:
                    stop["parent_station"] = stop["parent_station"].replace(feed_params.get("location_left_strip", ""), "", 1)
                else:
                    stop["parent_station"] = None
                # TODO: VARIABLE PREFIX
                loc = OrderedDict([
                    ("tiploc", prefix + stop["stop_id"]),
                    ("tiploc_bare", prefix + stop["stop_id"]),
                    ("operator", None),  # TODO: variable operator
                    ("category", feed_params.get("force_location_category", None) or  category_for(stop)),
                    ("wheelchair_boarding", stop.get("wheelchair_boarding")),
                    ("level_id", stop.get("level_id") or None),
                    ("stop_code", stop.get("stop_code") or None),
                    ("platform_code", stop.get("platform_code") or None),
                    ("parent_station", stop.get("parent_station") or None)
                    ])
                loc.update(OrderedDict([
                    ("name_short", stop["stop_name"] or stop["stop_name"]),
                    ("name_full", stop["stop_name"] or stop["stop_name"]),
                    ]))

                statement = insert(models.DarwinLocation.__table__)
                statement = statement.on_conflict_do_update(index_elements=(models.DarwinLocation.tiploc,), set_={
                    "tiploc": statement.excluded.tiploc,
                    "operator": statement.excluded.operator,
                    "category": statement.excluded.category,
                    "name_short": statement.excluded.name_short,
                    "name_full": statement.excluded.name_full,
                    "dict": statement.excluded.dict,
                    "wheelchair_boarding": statement.excluded.wheelchair_boarding,
                    "level_id": statement.excluded.level_id,
                    "platform_code": statement.excluded.platform_code,
                    "source": statement.excluded.source,
                    "platform_parent": statement.excluded.platform_parent
                    })
                db_c.sa_connection.execute(statement, tiploc=loc["tiploc"], operator=loc["operator"],
                                           name_short=loc["name_short"], name_full=loc["name_full"], dict=json.dumps(loc),
                                           wheelchair_boarding=loc["wheelchair_boarding"], level_id=loc["level_id"],
                                           platform_code=loc["platform_code"], category=loc["category"],
                                           source=version_ident,
                                           platform_parent=(loc["parent_station"] if loc.get("platform_code") else None))

                for key, prefix, id_type, source, name in [("stop_code",  prefix + "short:", "group", "naptan", loc["name_short"]),
                                             ("parent_station", prefix, "parent", "naptan", loc["name_short"]),
                                             ("tiploc", "", "self", "swallow", loc["name_full"])]:
                    if loc[key]:
                        # TODO: update more, this is just enough to avoid triggering accidental mass deletion of these rows
                        statement = insert(models.SwallowLocationID.__table__)
                        statement = statement.on_conflict_do_update(index_elements=(
                            models.SwallowLocationID.tiploc,
                            models.SwallowLocationID.secondary_id,
                            models.SwallowLocationID.secondary_id_type
                        ), set_={"source": statement.excluded.source})
                        db_c.sa_connection.execute(statement, tiploc=loc["tiploc"], secondary_id=prefix + loc[key],
                                                   secondary_id_type=id_type, source=version_ident, name=name)

    with open(file_location + "agency.txt") as f:
        for agency in _unbommed_dictreader(f):
            operator_id = agency["agency_id"].replace(feed_params.get("operator_left_strip", ""), "", 1)
            statement = insert(models.DarwinOperator.__table__)
            statement = statement.on_conflict_do_update(index_elements=(models.DarwinOperator.operator,),
                                                        set_={"operator_name": statement.excluded.operator_name,
                                                              "language": statement.excluded.language,
                                                              "url": statement.excluded.url,
                                                              "category": statement.excluded.category,
                                                              "source": statement.excluded.source,
                                                              "phone_no": statement.excluded.phone_no,
                                                              "fare_url": statement.excluded.fare_url,
                                                              "email": statement.excluded.email})
            db_c.sa_connection.execute(statement,
                                       operator=feed_params["operator_namespace"] + ":" + operator_id,
                                       operator_name=agency["agency_name"], url=agency["agency_url"],
                                       category=(feed_params.get("force_operator_category") or "O"),
                                       language=agency.get("agency_lang"), source=version_ident,
                                       phone_no=agency.get("agency_phone"), fare_url=agency.get("agency_fare_url"))
            # TODO: the way time is handled in this is not good
            agency_tz = agency["agency_timezone"]

    with open(file_location + "routes.txt") as f:
        for route in _unbommed_dictreader(f):
            operator_id = agency["agency_id"].replace(feed_params.get("operator_left_strip", ""), "", 1)
            statement = insert(models.SwallowRoute.__table__)
            statement = statement.on_conflict_do_update(index_elements=(models.SwallowRoute.route_id,),
                                                        set_={"operator_id": statement.excluded.operator_id,
                                                              "name_full": statement.excluded.name_full,
                                                              "name_short": statement.excluded.name_short,
                                                              "transport_category": statement.excluded.transport_category,
                                                              "source": statement.excluded.source,
                                                              })
            db_c.sa_connection.execute(statement,
                                       route_id=feed_params["service_namespace"] + ":" + route["route_id"],
                                       operator_id=feed_params["operator_namespace"] + ":" + operator_id,
                                       name_full=route.get("route_long_name") or route.get("route_short_name"),
                                       name_short=route.get("route_short_name") or route.get("route_long_name"),
                                       transport_category=route["route_type"],
                                       source=version_ident,
                                        )

    with open(file_location + "trips.txt") as f:
        for trip in _unbommed_dictreader(f):
            statement = insert(models.SwallowTrip.__table__)
            statement = statement.on_conflict_do_update(index_elements=(models.SwallowTrip.trip_id,),
                                                        set_={"route_id": statement.excluded.route_id,
                                                              "service_id": statement.excluded.service_id,
                                                              "trip_headsign": statement.excluded.trip_headsign,
                                                              "trip_short_name": statement.excluded.trip_short_name,
                                                              "wheelchair_accessible": statement.excluded.wheelchair_accessible,
                                                              "bikes_allowed": statement.excluded.bikes_allowed,
                                                              "source": statement.excluded.source,
                                                              })
            db_c.sa_connection.execute(statement,
                                       trip_id=feed_params["service_namespace"] + ":" + trip["trip_id"],
                                       route_id=feed_params["service_namespace"] + ":" + trip["route_id"],
                                       service_id=feed_params["service_namespace"] + ":" + trip["service_id"],
                                       trip_headsign=trip.get("trip_headsign"),
                                       trip_short_name=trip.get("trip_short_name"),
                                       wheelchair_accessible=trip.get("wheelchair_accessible"),
                                       bikes_allowed=trip.get("bikes_allowed"),
                                       source=version_ident,
                                        )

    if os.path.exists(file_location + "calendar.txt"):
        with open(file_location + "calendar.txt") as f:
            for calendar in _unbommed_dictreader(f):
                statement = insert(models.SwallowServiceWeekdayValidity.__table__)
                statement = statement.on_conflict_do_update(index_elements=(models.SwallowServiceWeekdayValidity.service_id,),
                                                            set_={"service_id": statement.excluded.service_id,
                                                                  "start_date": statement.excluded.start_date,
                                                                  "end_date": statement.excluded.end_date,
                                                                  "source": statement.excluded.source,
                                                                  "monday": statement.excluded.monday,
                                                                  "tuesday": statement.excluded.tuesday,
                                                                  "wednesday": statement.excluded.wednesday,
                                                                  "thursday": statement.excluded.thursday,
                                                                  "friday": statement.excluded.friday,
                                                                  "saturday": statement.excluded.saturday,
                                                                  "sunday": statement.excluded.sunday,
                                                                  })
                db_c.sa_connection.execute(statement,
                                           service_id=feed_params["service_namespace"] + ":" + calendar["service_id"],
                                           monday=bool(int(calendar["monday"])),
                                           tuesday=bool(int(calendar["tuesday"])),
                                           wednesday=bool(int(calendar["wednesday"])),
                                           thursday=bool(int(calendar["thursday"])),
                                           friday=bool(int(calendar["friday"])),
                                           saturday=bool(int(calendar["saturday"])),
                                           sunday=bool(int(calendar["sunday"])),
                                           start_date=calendar["start_date"],
                                           end_date=calendar["end_date"],
                                           source=version_ident,
                                            )

    if os.path.exists(file_location + "calendar_dates.txt"):
        with open(file_location + "calendar_dates.txt") as f:
            for calendar_day in _unbommed_dictreader(f):
                statement = insert(models.SwallowServiceDayValidity.__table__)
                statement = statement.on_conflict_do_update(index_elements=(models.SwallowServiceDayValidity.service_id,
                                                                            models.SwallowServiceDayValidity.date),
                                                            set_={"service_id": statement.excluded.service_id,
                                                                  "date": statement.excluded.date,
                                                                  "exception_type": statement.excluded.exception_type,
                                                                  "source": statement.excluded.source
                                                                  })
                db_c.sa_connection.execute(statement,
                                           service_id=feed_params["service_namespace"] + ":" + calendar_day["service_id"],
                                           date=calendar_day["date"],
                                           exception_type=calendar_day["exception_type"],
                                           source=version_ident,
                                            )

    with open(file_location + "stop_times.txt") as f:
        for stop_time in _unbommed_dictreader(f):
            statement = insert(models.SwallowStopTime.__table__)
            statement = statement.on_conflict_do_update(index_elements=(models.SwallowStopTime.trip_id,
                                                                        models.SwallowStopTime.stop_sequence),
                                                        set_={"trip_id": statement.excluded.trip_id,
                                                              "arrival_time": statement.excluded.arrival_time,
                                                              "departure_time": statement.excluded.departure_time,
                                                              "stop_id": statement.excluded.stop_id,
                                                              "source": statement.excluded.source
                                                              })
            db_c.sa_connection.execute(statement,
                                       trip_id=feed_params["service_namespace"] + ":" + stop_time["trip_id"],
                                       arrival_time=time_as_secs(stop_time["arrival_time"]),
                                       departure_time=time_as_secs(stop_time["departure_time"]),
                                       stop_id=feed_params["location_namespace"] + ":" + stop_time["stop_id"],
                                       stop_sequence=stop_time["stop_sequence"],
                                       source=version_ident,
                                        )

    statement = text("""UPDATE swallow_trips
                            SET first_time=(
                                SELECT departure_time
                                    FROM swallow_stop_times
                                    WHERE swallow_stop_times.trip_id=swallow_trips.trip_id
                                    ORDER BY stop_sequence ASC LIMIT 1);
                                """)
    db_c.sa_connection.execute(statement)


    # Update when we picked up the data
    statement = insert(models.SwallowFeed.__table__)
    statement = statement.on_conflict_do_update(index_elements=(models.SwallowFeed.feed_id,),
                                                set_={"feed_version": statement.excluded.feed_version,
                                                   "sequence": statement.excluded.sequence,
                                                   "time_acquired": statement.excluded.time_acquired,
                                                   "capabilities": statement.excluded.capabilities,
                                                   "timezone": statement.excluded.timezone,
                                                      })
    db_c.sa_connection.execute(statement, feed_id=feed_params["id"], feed_version=version_ident, sequence=0,
                               time_acquired=utcnow, capabilities="T", parent_feed_id=None, timezone=agency_tz)


def renew_gtfs(c):
    for feed in config.get("modules")["gtfs"]["feeds"]:
        if feed.get("enabled", True):
            log.info("Attempting to renew GTFS feed " + feed["id"])
            try:
                shutil.rmtree("/tmp/ironswallow_gtfs_ex/")
            except FileNotFoundError:
                pass

            req = requests.get(feed["url"])
            target_filename = "/tmp/ironswallow_gtfs.zip"

            with open(target_filename, "wb") as f:
                f.write(req.content)

            ZipFile("/tmp/ironswallow_gtfs.zip").extractall("/tmp/ironswallow_gtfs_ex/")
            store(c, "/tmp/ironswallow_gtfs_ex/", feed)


def activate_gtfs(db_c: DatabaseConnection):
    config_feeds = {feed["id"]: feed for feed in config.get("modules")["gtfs"]["feeds"]}

    statement = select(models.SwallowFeed.__table__)
    for feed in list(db_c.sa_connection.execute(statement)):
        config_feed = config_feeds.get(feed["feed_id"])
        if config_feed and config_feed.get("enabled", True):
            activated_time = max(feed["time_activated"] or 0, (datetime.utcnow()-timedelta(minutes=30)).timestamp())
            if feed["time_activated"] and activated_time-feed["time_activated"] > 30*60:  # 30 min
                log.warning("Feed {} activating with interval over 30 minutes".format(feed["feed_id"]))

            tz_dt = datetime.now(tz=pytz.timezone(feed["timezone"]))

            lower_bound_dt = datetime.fromtimestamp(activated_time).astimezone(pytz.timezone(feed["timezone"]))
            upper_bound_dt = tz_dt + timedelta(hours=9)

            lower_bound = dt_as_day_secs(lower_bound_dt)
            upper_bound = dt_as_day_secs(upper_bound_dt)

            result = db_c.sa_connection.execute(text("""
            SELECT * FROM swallow_trips
                WHERE
                    swallow_trips.source=:source AND
                    (
                    service_id IN
                        (SELECT service_id FROM swallow_weekday_validities
                            WHERE start_date <= :today AND end_date >= :today AND {}=true)
                    OR service_id IN
                        (SELECT service_id FROM swallow_date_validities
                            WHERE date=:today AND exception_type=1)
                    )
                    AND service_id NOT IN
                        (SELECT service_id FROM swallow_date_validities
                            WHERE date=:today AND exception_type=2)
                    AND 
                        swallow_trips.first_time > :period_start
                    AND
                        swallow_trips.first_time <= :period_end
                        
                    ORDER BY swallow_trips.first_time ASC;
                    """.format(ISO_WEEKDAYS[tz_dt.isoweekday()])), source=feed["feed_version"],
                                       today=tz_dt.date().isoformat(), period_start=lower_bound, period_end=upper_bound)

            for trip in result:
                basis_date = tz_dt.date()
                if trip["first_time"] > 86400:
                    basis_date += timedelta(days=trip["first_time"] // 86400)

                trip_id_parts = trip["trip_id"].split(":", 1)
                pseudo_rid = trip_id_parts[0] + ":" + basis_date.isoformat().replace("-", "") + "_" + trip_id_parts[1]

                origins = []
                destinations = []
                locations = []

                result = db_c.sa_connection.execute(
                    text("""SELECT * FROM swallow_stop_times WHERE trip_id=:trip_id ORDER BY stop_sequence ASC;"""),
                    trip_id=trip["trip_id"])
                for idx, location in enumerate(result):
                    loc_out = {
                        "rid": pseudo_rid,
                        "index": idx,
                        "type": {0: "OR", result.rowcount-1: "DT"}.get(idx, "IP"),
                        "activity": {0: "TB", result.rowcount-1: "TF"}.get(idx, "T"),
                        "tiploc": location["stop_id"],
                        "wta": day_secs_as_dt(tz_dt, location["arrival_time"] or 0),
                        "pta": day_secs_as_dt(tz_dt, location["arrival_time"] or 0),
                        "wtd": day_secs_as_dt(tz_dt, location["departure_time"] or 0),
                        "ptd": day_secs_as_dt(tz_dt, location["departure_time"] or 0),
                    }

                    locations.append(loc_out)

                    loc_summary_dict = OrderedDict([
                        ("source", "SC"), ("type", loc_out["type"]), ("activity", loc_out["activity"]),
                     ("cancelled", False)])

                    loc_summary_dict.update(json.loads(
                        db_c.sa_connection.execute(
                            text("SELECT * FROM darwin_locations WHERE tiploc=:tiploc"),
                            tiploc=location["stop_id"]).fetchone()["dict"]
                    ))

                    if loc_out["type"] == "OR":
                        origins.append(loc_summary_dict)
                    if loc_out["type"] == "DT":
                        destinations.append(loc_summary_dict)


                statement = insert(models.DarwinSchedule.__table__)
                statement = statement.on_conflict_do_update(index_elements=(models.DarwinSchedule.rid,), set_={
                    "signalling_id": statement.excluded.signalling_id,
                    "status": statement.excluded.status,
                    "category": statement.excluded.category,
                    "operator": statement.excluded.operator,
                    "origins": statement.excluded.origins,
                    "destinations": statement.excluded.destinations,
                })
                # TODO: category, operator
                db_c.sa_connection.execute(statement,
                                           rid=pseudo_rid,
                                           uid=trip["trip_id"],
                                           ssd=basis_date,
                                           signalling_id="0000",
                                           status="P",
                                           category="aa",
                                           operator="aa",
                                           origins=origins,
                                           destinations=destinations,)

                db_c.sa_connection.execute(insert(models.DarwinScheduleLocation.__table__), locations)
