from decimal import Decimal
import datetime

def compare_time(t1, t2) -> int:
    if not (t1 and t2):
        return 0
    t1, t2 = [a.hour*3600+a.minute*60+a.second for a in (t1,t2)]
    return (Decimal(t1)-Decimal(t2))/3600


def process_time(time) -> datetime.time:
    if not time:
        return None
    if len(time) == 5:
        time += ":00"
    return datetime.datetime.strptime(time, "%H:%M:%S").time()


def full_original_wt(location):
    return form_original_wt([process_time(location.get(a)) for a in ("wta", "wtp", "wtd")])


def form_original_wt(times) -> str:
    out = ""
    for time in times:
        if time:
            out += time.strftime("%H%M%S")
        else:
            out += "      "
    return out
