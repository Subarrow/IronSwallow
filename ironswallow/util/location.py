FORCE_LOCATION_TZ = {
    # Eurostar
    "AMSTDAM": "Europe/Amsterdam",    # Amsterdam CS (NL)
    "MARNELV": "Europe/Paris",        # Marne-Le-Vallee (FR)
    "MRSLSTC": "Europe/Paris",        # Marseille St Charles (FR)
    "CALAFGV": "Europe/Paris",        # Calais Frethun Gare Vgrs (FR)
    "BRUXMID": "Europe/Brussels",     # Bruxelles Midi (BE)
    "PARISND": "Europe/Paris",        # Paris Gare du Nord (FR)
    "LILLE":   "Europe/Paris",        # Lille (FR)
    "AVIGCMT": "Europe/Paris",        # Avignon C M T (FR)
    "BORGSTM": "Europe/Paris",        # Bourg St. Maurice (FR)

    # Ferry
    "DUBLFST": "Europe/Dublin",       # Dublin Port-Stena (IE)
    "ROSLARE": "Europe/Dublin",       # Rosslare Harbour (IE)
    "DOUGLAS": "Europe/Isle_of_man",  # Douglas (Isle of Man) (IM)
    "HOEKVHL": "Europe/Amsterdam",    # Hoek Van Holand (NL)
    "DUNLGHR": "Europe/Dublin",       # Dun Laoghaire (IE)
    "DUBLINF": "Europe/Dublin",       # Dublin Ferryport (IE)
    "BLFSTPT": "Europe/Belfast",      # Belfast port (GB-NIR)

    # Ferry (UK)
    "PEMBFTM": "Europe/London",       # Pembroke Ferry Terminal (GB-WLS)
    "PRSTNCS": "Europe/London",       # Preston C.S. (GB-ENG)
    "CNRYNP":  "Europe/London",       # Cairnryan (Loch Ryan Port) (GB-SCT)
    "LVRPLSG": "Europe/London",       # Liverpool Landing Stage (GB-ENG)
    "YMTHIOW": "Europe/London",       # Yarmouth (I.O.W.) (GB-ENG)
}
