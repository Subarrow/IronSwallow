import datetime, json, re, logging, time, threading
from collections import OrderedDict
from decimal import Decimal
from queue import Queue
from queue import LifoQueue
from typing import Union

import pytz
import psycopg2.extras

from ironswallow.store import meta
from ironswallow.util import query
from ironswallow.util.darwin_util import (process_time, compare_time, form_original_wt, full_original_wt)
from main import LOCATIONS, REASONS
from ironswallow.util.location import FORCE_LOCATION_TZ

OBSERVED_LOCATIONS = set()

log = logging.getLogger("IronSwallow")

COACH_CLASS_SHORT = {
    "First": "1",
    "Standard": "2",
    "Composite": "C", # Hedging bets, these aren't present in data, either a carriage is first or sec- standard
    "Both": "C"
    }

TOILET_TYPE_SHORT = {
    "Accessible": "A",
    "Standard": "L",
}


def timecopy(input_dt: datetime.datetime) -> datetime.datetime:
    return datetime.datetime(input_dt.year, input_dt.month, input_dt.day,
                             input_dt.hour, input_dt.minute, input_dt.second)


def process_reason(reason):
    return OrderedDict([
        ("code", reason["$"]),
        ("message", REASONS[(reason["$"], "C"*(reason["tag"]=="cancelReason") or "D")]),
        ("location", query.process_location_outline(LOCATIONS.get("gb-nr:" + reason.get("tiploc", "")))),
        ("near", bool(reason.get("near"))),
        ])


class MessageProcessor:
    """In theory you can use this without the context manager, but don't"""

    def __init__(self, cursor):
        self.cursor = cursor
        self._query_queue = Queue(maxsize=1000)
        self._query_fetch = LifoQueue()
        self._thread_quit = False
        self._thread_start = False
        self._thread = None

    def count(self) -> int:
        return self._query_queue.qsize()

    def execute(self, query: str, params: Union[tuple, list]=(), batch=False, retain=False, use_retain=False):
        self._query_queue.put((query, params, batch, retain, use_retain))

    def __enter__(self) -> "MessageProcessor":
        self.thread = threading.Thread(target=self._execute_thread)
        self.thread.start()
        self._thread_start = True
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._query_queue.put(None)
        while not self._thread_quit:
            time.sleep(0.1)

        return False

    def _execute_thread(self):
        while True:
            entry = self._query_queue.get()
            if not entry:
                self._thread_quit = True
                return
            query, params, batch, retain, use_retain = entry

            if use_retain:
                params = self._query_fetch.get()

            if batch:
                psycopg2.extras.execute_batch(self.cursor, query, params)
            else:
                self.cursor.execute(query, params)

            if retain:
                self._query_fetch.put(self.cursor.fetchall())

    def store(self, parsed) -> None:
        global OBSERVED_LOCATIONS
        if not parsed:
            return

        assoc_batch = []

        for record in parsed:
            if record["tag"] == "schedule":

                index = 0
                last_time, ssd_offset = None, 0

                # I feel like I owe an explanation for this abomination, so here we go - it turns out that breaking a
                # foreign key reference by means other than straightforward deletion isn't something that you can handle
                # with constraints in psql. Ideally you could very neatly put aside something that didn't match back
                # up, but that's just not how it goes
                # The select here is so bizarre just so this can be fed direct back into the insert later on
                self.execute("SELECT category,tiploc,main_rid,main_original_wt,assoc_rid,assoc_original_wt, "
                          "tiploc,main_rid,main_original_wt,"
                          "tiploc,assoc_rid,assoc_original_wt "
                          "FROM darwin_associations WHERE main_rid=%s OR assoc_rid=%s", ("gb-nr:" + record["rid"], "gb-nr:" + record["rid"]), retain=True)

                self.execute("DELETE FROM darwin_schedule_locations WHERE rid=%s;", ("gb-nr:" + record["rid"],))

                origins, destinations = [], []
                batch = []

                for location in record["list"]:
                    if location["tag"] in ["OPOR", "OR", "OPIP", "IP", "PP", "DT", "OPDT"]:
                        OBSERVED_LOCATIONS |= {"gb-nr:" + location["tpl"]}

                        times = []
                        for time_n, time in [(a, location.get(a, None)) for a in ["pta", "wta", "wtp", "ptd", "wtd"]]:
                            if time:
                                if len(time)==5:
                                    time += ":00"
                                time = datetime.datetime.strptime(time, "%H:%M:%S").time()
                                # Crossed midnight, increment ssd offset
                                if compare_time(time, last_time) < -6:
                                    ssd_offset += 1
                                # Normal increase or decrease, nothing we really need to do here
                                elif -6 <= compare_time(time, last_time) <= +18:
                                    pass
                                # Back in time, crossed midnight (in reverse), decrement ssd offset
                                elif +18 < compare_time(time, last_time):
                                    ssd_offset -= 1

                                last_time = time
                                time = timecopy(pytz.timezone(FORCE_LOCATION_TZ.get(location["tpl"], "Europe/London")).localize(datetime.datetime.combine(datetime.datetime.strptime(record["ssd"], "%Y-%m-%d").date(), time)).astimezone(pytz.UTC) + datetime.timedelta(days=ssd_offset))
                            times.append(time)

                        original_wt = form_original_wt([process_time(location.get(a)) for a in ("wta", "wtp", "wtd")])

                        batch.append(("gb-nr:" + record["rid"], index, location["tag"], "gb-nr:" + location["tpl"], location.get("act", ''), original_wt, *times, bool(location.get("can")), location.get("rdelay", 0)))

                        loc_dict = OrderedDict([("source", "SC"), ("type", location["tag"]), ("activity", location.get("act",'')), ("cancelled", bool(location.get("can")))])
                        loc_dict.update(LOCATIONS["gb-nr:" + location["tpl"]])

                        if location["tag"] in ("OR", "OPOR"):
                            origins.append(json.dumps(loc_dict))
                        if location["tag"] in ("DT", "OPDT"):
                            destinations.append(json.dumps(loc_dict))

                        index += 1

                    elif location["tag"]=="cancelReason":
                        self.execute("UPDATE darwin_schedules SET cancel_reason=%s WHERE rid=%s;", (json.dumps(process_reason(location)), "gb-nr:" + record["rid"]))

                self.execute("""INSERT INTO darwin_schedules VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s::json[], %s::json[], %s, %s)
                    ON CONFLICT (rid) DO UPDATE SET
                    signalling_id=EXCLUDED.signalling_id, status=EXCLUDED.status, category=EXCLUDED.category,
                    operator=EXCLUDED.operator, is_active=EXCLUDED.is_active, is_charter=EXCLUDED.is_charter,
                    is_deleted=EXCLUDED.is_deleted, is_passenger=EXCLUDED.is_passenger, origins=EXCLUDED.origins, destinations=EXCLUDED.destinations;""", (
                    record["uid"], "gb-nr:" + record["rid"], record.get("rsid"), record["ssd"], record["trainId"],
                    record.get("status") or "P", record.get("trainCat") or "OO", "gb-nr:" + record["toc"], record.get("isActive") or True,
                    bool(record.get("isCharter")), bool(record.get("deleted")), record.get("isPassengerSvc") or True,
                    origins, destinations, "gb-darwin", "gb-darwin:0"
                    ))

                self.execute("""INSERT INTO darwin_schedule_locations VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING;""", params=batch, batch=True)

                self.execute("""INSERT INTO darwin_associations
                (category,tiploc,main_rid,main_original_wt,assoc_rid,assoc_original_wt) SELECT %s,%s,%s,%s,%s,%s WHERE
                EXISTS (SELECT * FROM darwin_schedule_locations WHERE tiploc=%s AND rid=%s AND original_wt=%s) AND
                EXISTS (SELECT * FROM darwin_schedule_locations WHERE tiploc=%s AND rid=%s AND original_wt=%s) ON CONFLICT DO NOTHING;""", batch=True, use_retain=True)

            elif record["tag"] == "TS":
                last_time, ssd_offset = None, 0

                batch = []
                for location in record["list"]:
                    original_wt = form_original_wt([process_time(location.get(a)) for a in ("wta", "wtp", "wtd")])
                    if location["tag"] == "Location":
                        times = []
                        times_source = []
                        times_type = []
                        times_delay = []

                        # TODO: verify against feed dumps whether TS updates are usually complete (in which case this
                        # TODO: is ok) or not (in which case it is a problem)
                        for time_name in ["arr", "pass", "dep"]:
                            time_d = location.get(time_name, {})
                            time_content = time_d.get("at", None) or time_d.get("et", None)
                            time = None
                            if time_content:
                                time = process_time(time_content)
                                working_time = process_time(location["wt" + time_name[0]])

                                if working_time and not last_time:
                                    last_time = working_time  # to guard against wrong offsets around midnight

                                # Crossed midnight, increment ssd offset
                                if compare_time(time, last_time) < -6:
                                    ssd_offset += 1
                                # Normal increase or decrease, nothing we really need to do here
                                elif -6 <= compare_time(time, last_time) <= +18:
                                    pass
                                # Back in time, crossed midnight (in reverse), decrement ssd offset
                                elif +18 < compare_time(time, last_time):
                                    ssd_offset -= 1

                                last_time = working_time
                                time = timecopy(pytz.timezone(FORCE_LOCATION_TZ.get(location["tpl"], "Europe/London")).localize(datetime.datetime.combine(datetime.datetime.strptime(record["ssd"], "%Y-%m-%d").date(), time)).astimezone(pytz.UTC) + datetime.timedelta(days=ssd_offset))


                            times.append(time)
                            times_source.append(time_d.get("src"))
                            times_type.append("E"*("et" in time_d) or "A"*("at" in time_d) or None)
                            times_delay.append(bool(time_d.get("delayed")))

                        plat = location.get("plat", {})
                        batch.append((
                            *times, *times_type, *times_delay,
                            plat.get("$"), bool(plat.get("platsup")), bool(plat.get("cisPlatsup")), bool(plat.get("conf")),
                            location.get("length", {}).get("$"),
                            "gb-nr:" + record["rid"], "gb-nr:" + location["tpl"], original_wt,))

                    if location["tag"]=="LateReason":
                        self.execute("UPDATE darwin_schedules SET delay_reason=%s WHERE rid=%s;", (json.dumps(process_reason(location)), "gb-nr:" + record["rid"]))

                self.execute("""UPDATE darwin_schedule_locations
                    SET (ta,tp,td, ta_type,tp_type,td_type, ta_delayed,tp_delayed,td_delayed,
                    plat, plat_suppressed, plat_cis_suppressed, plat_confirmed, length)=(
                    %s,%s,%s, %s,%s,%s, %s,%s,%s, %s,%s,%s,%s, %s) WHERE 
                    rid=%s AND tiploc=%s AND original_wt=%s""", params=batch, batch=True)

            elif record["tag"]=="deactivated":
                self.execute("UPDATE darwin_schedules SET is_active=FALSE WHERE rid=%s;", ("gb-nr:" + record["rid"],))
            elif record["tag"]=="OW":
                station_list = ["gb-nr:" + a["crs"] for a in record["list"] if a["tag"] == "Station"]


                message = [a.get("$") or '' for a in record["list"] if a["tag"]=="Msg"][0]

                # Some messages are enclosed in <p> tags, some have a <p></p> in them.
                # Thank you National Rail, very cool
                pattern = pattern = re.compile("(^<p>)|(</p>$)")
                message = pattern.sub("", message).replace("<p></p>", "").replace('</p><p>', '<br>')

                if station_list:
                    self.execute("""INSERT INTO darwin_messages VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (message_id)
                        DO UPDATE SET (category, severity, suppress, stations, message)=
                        (EXCLUDED.category, EXCLUDED.severity, EXCLUDED.suppress, EXCLUDED.stations, EXCLUDED.message);""",
                        (record["id"], record["cat"], record["sev"], bool(record.get("suppress")), station_list, message))
                else:
                    self.execute("DELETE FROM darwin_messages WHERE message_id=%s;", (record["id"],))

            elif record["tag"] == "association":
                main_owt = full_original_wt(record["main"])
                assoc_owt = full_original_wt(record["assoc"])
                record["tiploc"] = "gb-nr:" + record["tiploc"]
                if record["category"] == "JJ":
                    # Semantically it makes a lot more sense to invert joins, so that all associations point to the "next" service
                    # JN should hopefully make this distinct from JJ
                    # The subclauses here replace a slightly nicer set of queries which notified about orphan assocs. Oh well.
                    assoc_batch.append(("JN", record["tiploc"], "gb-nr:" + record["assoc"]["rid"], assoc_owt, "gb-nr:" + record["main"]["rid"], main_owt,
                                        record["tiploc"], "gb-nr:" + record["main"]["rid"], main_owt,
                                        record["tiploc"], "gb-nr:" + record["assoc"]["rid"], assoc_owt))
                else:
                    assoc_batch.append((record["category"], record["tiploc"], "gb-nr:" + record["main"]["rid"], main_owt, "gb-nr:" + record["assoc"]["rid"], assoc_owt,
                                        record["tiploc"], "gb-nr:" + record["main"]["rid"], main_owt,
                                        record["tiploc"], "gb-nr:" + record["assoc"]["rid"], assoc_owt))
            elif record["tag"] == "scheduleFormations":
                self.execute("DELETE FROM darwin_formations WHERE rid=%s;", ("gb-nr:" + record["rid"],))
                formation_summaries = []
                coach_batch = []
                seq = 0
                if "formation" not in record:
                    log.error("Formation not in formation record")
                    continue
                for formation in record["formation"]:
                    unit_coaches = OrderedDict()
                    for coach in formation["coaches"]["coach"]:
                        # Put the coach characteristics in nice variables
                        coach_number = coach["coachNumber"]
                        coach_class = coach["coachClass"]

                        if "toilet" in coach:
                            coach_toilet_status = coach["toilet"].get("status")
                            coach_toilet_type = coach["toilet"]["$"]
                        else:
                            coach_toilet_status, coach_toilet_type = "Unknown", "None"

                        # Make a summary from it
                        summary_part_1 = COACH_CLASS_SHORT.get(coach_class, "?")
                        summary_part_2 = TOILET_TYPE_SHORT.get(coach_toilet_type, "")
                        summary_part_3 = "*"*(coach_toilet_status == "NotInService")

                        coach_prefix = coach_number[0] if coach_number[0].isalpha() else ""
                        unit_coaches[coach_prefix] = unit_coaches.get(coach_prefix) or []

                        unit_coaches[coach_prefix].append(summary_part_1 + summary_part_2 + summary_part_3)

                        coach_batch.append(("gb-nr:" + record["rid"], formation["fid"], seq, coach_number, coach_class,
                                    coach_toilet_status, coach_toilet_type))
                        seq += 1

                    formation_summaries.append("=".join(["-".join(a) for a in unit_coaches.values()]))

                self.execute("INSERT INTO darwin_formations VALUES (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING;",
                             coach_batch, batch=True)
                self.execute("UPDATE darwin_schedules SET formation_summary=%s WHERE rid=%s;",
                             (" / ".join(formation_summaries), "gb-nr:" + record["rid"]))
            else:
                log.debug(record)
        if assoc_batch:
            self.execute("""INSERT INTO darwin_associations SELECT %s, %s, %s, %s, %s, %s WHERE
                                EXISTS (SELECT * FROM darwin_schedule_locations WHERE tiploc=%s AND rid=%s AND original_wt=%s) AND
                                EXISTS (SELECT * FROM darwin_schedule_locations WHERE tiploc=%s AND rid=%s AND original_wt=%s)
                                ON CONFLICT(tiploc,main_rid,assoc_rid) DO NOTHING;""", assoc_batch, batch=True)

        if not self._thread_start:
            self._query_queue.put(None)
            self._execute_thread()
