from collections import OrderedDict
import json
import logging

from main import REASONS, LOCATIONS
from . import category
from . import names

from ironswallow.bplan import LOCALISED_OTHER_REFERENCES, BPLAN_NAMES
from ironswallow.util.location import FORCE_LOCATION_TZ

logger = logging.getLogger("IronSwallow")

LOCALISED_OTHER_REFERENCES.extend([
    ("IS", "en_gb", "OPCAT", "S", "Mainline operator"),
    ("IS", "en_gb", "OPCAT", "M", "Non-mainline rail operator"),
    ("IS", "en_gb", "OPCAT", "O", "Non-rail operator"),
    ("IS", "en_gb", "OPCAT", "C", "Charter operator"),
    ("IS", "en_gb", "OPCAT", "B", "BPLAN operator (do not use)"),

    ("IS", "nb_no", "OPCAT", "S", "Hovedlinje togoperatør"),
    ("IS", "nb_no", "OPCAT", "M", "Ikke-hovedlinje togoperatør"),
    ("IS", "nb_no", "OPCAT", "O", "Ikke-jernbane operatør"),
    ("IS", "nb_no", "OPCAT", "C", "Chartertogoperatør"),
    ("IS", "nb_no", "OPCAT", "B", "Operatør fra BPLAN (bruk ikke)")
    ])

def toc_category_for(toc):
    if toc in ["NY", "PC", "ZM", "WR"]:
        return "C"
    if toc in ["LT", "SJ", "TW"]:
        return "M"
    if toc in ["ZB", "ZF"]:
        return "O"
    return "S"

def store(c, parsed) -> None:
    strip = lambda x: x.rstrip() or None if x else None

    c.execute("DELETE FROM swallow_debug WHERE subsystem='NSUB';")

    with open("datasets/corpus.json", encoding="iso-8859-1") as f:
        corpus = json.load(f)["TIPLOCDATA"]
    corpus = {a["TIPLOC"]: a for a in corpus}

    for reference in parsed["PportTimetableRef"]["list"]:
        if reference["tag"] == "LocationRef":
            corpus_loc = corpus.get(reference["tpl"], {})

            loc = OrderedDict([
                ("tiploc", "gb-nr:" + reference["tpl"]),
                ("tiploc_bare", reference["tpl"]),
                ("crs_darwin", reference.get("crs")),
                ("crs_corpus", strip(corpus_loc.get("3ALPHA"))),
                ("operator", reference.get("toc")),
                ("name_darwin", reference["locname"]*(reference["locname"] != reference["tpl"]) or None),
                ("name_corpus", strip(corpus_loc.get("NLCDESC"))),
                ("name_bplan", BPLAN_NAMES.get(reference["tpl"])),
                ("category", None),
                ("wheelchair_boarding", None),
                ("level_id", None),
            ])
            loc.update(OrderedDict([
                ("name_short", loc["name_darwin"] or loc["name_corpus"]),
                ("name_full", loc["name_corpus"] or loc["name_darwin"]),
                ]))

            loc["category"] = category.category_for(loc)
            loc["name_short"], loc["name_full"] = names.name_for(loc, c)

            timezone = FORCE_LOCATION_TZ.get(reference["tpl"])
            if reference.get("toc") in ["ES", "ZF"] and not timezone:
                logger.warning(f"Darwin location {reference['tpl']} could be int'l, but has no specified timezone")
            else:
                timezone = timezone or "Europe/London"

            c.execute("""INSERT INTO darwin_locations VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                ON CONFLICT(tiploc) DO UPDATE SET
                (tiploc, operator, category, name_short, name_full, dict, wheelchair_boarding, level_id, source,
                timezone)=
                (EXCLUDED.tiploc, EXCLUDED.operator, EXCLUDED.category,
                EXCLUDED.name_short, EXCLUDED.name_full, EXCLUDED.dict, EXCLUDED.wheelchair_boarding, EXCLUDED.level_id,
                EXCLUDED.source, EXCLUDED.timezone);
                """, (loc["tiploc"], loc["operator"], loc["name_short"], loc["name_full"], json.dumps(loc),
                      loc["category"], loc["wheelchair_boarding"], loc["level_id"], None, "darwin-reference", None,
                      timezone
                      ))
            for key, prefix, id_type, source, name in [("crs_darwin", "gb-nr:", "group", "darwin", loc["name_darwin"]),
                                         ("crs_corpus", "gb-nr:bplan:", "group", "bplan", loc["name_bplan"]),
                                         ("tiploc", "", "self", "swallow", loc["name_full"])]:
                if loc[key]:
                    c.execute("""INSERT INTO swallow_location_ids VALUES (%s, %s, %s, %s, %s) ON CONFLICT DO NOTHING""", (
                        loc["tiploc"], prefix + loc[key], id_type, source, name
                    ))

            LOCATIONS[loc["tiploc"]] = loc

        if reference["tag"] == "TocRef":
            c.execute("""INSERT INTO darwin_operators VALUES (%s, %s, %s, %s, %s, %s) ON CONFLICT (operator)
                DO UPDATE SET (operator_name, url, category, language, source)=(EXCLUDED.operator_name, EXCLUDED.url, EXCLUDED.category,
                EXCLUDED.language, EXCLUDED.source);""",
                ("gb-nr:" + reference["toc"], reference["tocname"], reference.get("url"), toc_category_for(reference["toc"]),
                 "en-GB", "darwin-reference"))

        if reference["tag"] in ["CancellationReasons", "LateRunningReasons"]:
            reason_type = "C"*(reference["tag"] == "CancellationReasons") or "D"
            for reason in reference["list"]:
                if reason["tag"] == "Reason":
                    c.execute("""INSERT INTO darwin_reasons VALUES (%s, %s, %s) ON CONFLICT (id, type) DO UPDATE
                        SET (type, message)=(EXCLUDED.type, EXCLUDED.message);""",
                        (reason["code"], reason_type, reason["reasontext"]))
                    REASONS[(reason["code"], reason_type)] = reason["reasontext"]

    c.execute("COMMIT;")
