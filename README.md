# IronSwallow

IronSwallow consumes National Rail's Darwin push port for passenger rail data,
with a RESTful API intended to offer a practical (and much more generously
billed) alternative to the official SOAP LDB(SV)WS that National Rail also offers

## Licence
This project is licenced under the GNU GPL, version 3 (for now).

The avatar for this project was commissioned from [L James](https://l-james.com/), and is
licensed as CC BY-NC 4.0.

## Setup
In order to use IronSwallow, you'll need a National Rail open data account
(sign up [here](https://opendata.nationalrail.co.uk/)). Make sure Darwin is
enabled in your account, copy `secret.json.example` to `secret.json`, and fill
in the credentials there. This may be a little tedious.

Make sure you've got a PostgreSQL database on hand, fill in the connection
string in the secret file.

You don't need to do anything special to initialise the database if you haven't run it before, IronSwallow
will generate the tables

If you don't already have the dependencies, installing them might be useful
(`pip3 install --user -r requirements.txt`)

There's presently two components you may wish to run. The first is the consumer
(`./iron-swallow.py`), and the second is [CopperSwallow](https://github.com/EvelynSubarrow/CopperSwallow),
which offers a web interface.

## Dependencies
* [psycopg2](https://pypi.org/project/psycopg2/)
* [stomp.py](https://pypi.org/project/stomp.py/)
* [boto3](https://pypi.org/project/boto3/)
* [pytz](https://pypi.org/project/pytz/)
